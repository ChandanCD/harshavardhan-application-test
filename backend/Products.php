<?php

include_once realpath(dirname(__FILE__)) . '\Helper.php';

class Products extends Helper {
    public static function getProducts(){
        $filename = "data.csv";
        $data = self::csv_to_array($filename);
        $resp = [
            "status" => "success",
            "data" => $data
        ];
        self::returnResponse($resp);
    }

    public static function addProduct() {
        $request = self::getPost();
    
        $filename = "data.csv";
        $data = self::csv_to_array($filename);
        $keys = array_column($data, 'name');
        $index = array_search($request['name'], $keys);

        if($index === false) {
            $newRow = [];
            $newRow["id"] = count($data) ? ($data[count($data)-1]["id"] + 1)  : 1;
            $newRow["name"] = $request["name"];
            $newRow["state"] = $request["state"];
            $newRow["zip"] = $request["zip"];
            $newRow["amount"] = $request["amount"];
            $newRow["qty"] = $request["qty"];
            $newRow["item"] = $request["item"];
            $fp = fopen($filename, 'a');    
            fputcsv($fp, array_values($newRow));

            $data[] = $newRow;
            $resp = [
                "status" => "success",
                "message" => "Products added successfully",
                "data" => $data
            ];
        } else {
            $resp = [
                "status" => "error",
                "message" => "Product already exists",
                "data" => $data
            ];

        }
        self::returnResponse($resp);
    }

    public static function updateProduct() {
        $request = self::getPost();
    
        $filename = "data.csv";
        $data = self::csv_to_array($filename);
        $keys = array_column($data, 'id');
        $index = array_search($request['id'], $keys);
        if($index !== false) {

            $data[$index] = $request; // updates
            $fp = fopen($filename, 'w');    
            fputcsv($fp, array_keys($data[0]));
            foreach ($data as $rows) {
                fputcsv($fp, array_values($rows));
            } 
        
            $resp = [
                "status" => "success",
                "message" => "Product updated successfully",
                "data" => $data
            ];
            
        } else {
            $resp = [
                "status" => "error",
                "message" => "No product found",
                "data" => $data
            ];

        }
        self::retrunResponse($resp);
    }

    public static function deleteProduct() {
        $request = self::getPost();
        
        $filename = "data.csv";
        $data = self::csv_to_array($filename);
        $keys = array_column($data, 'id');
        $index = array_search($request['id'], $keys);
        if($index !== false) {
            $fp = fopen($filename, 'w');
            fputcsv($fp, array_keys($data[0]));
    
            array_splice($data, $index, 1); // deletes
            foreach ($data as $rows) {
                    fputcsv($fp, array_values($rows));
            } 
        
            $resp = [
                "status" => "success",
                "data" => $data
            ];
        } else {
            $resp = [
                "status" => "error",
                "message" => "No product found",
                "data" => $data
            ];
    
        }
        self::retrunResponse($resp);

    }

}