<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    return false;    // serve the requested resource as-is.
} 

require_once(realpath(dirname(__FILE__)) . '\Products.php');

$route = str_replace("/index.php/","",$_SERVER['PHP_SELF']);

if($_SERVER["REQUEST_METHOD"] == "GET") {
    if($route == "api/products/all") Products::getProducts();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if($route == "api/products")  Products::addProduct() ;
    
    if($route == "api/products/update") Products::updateProduct();
    
    if($route == "api/products/delete") Products::deleteProduct();
}

header("HTTP/1.0 404 Not Found");

