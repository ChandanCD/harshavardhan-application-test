import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyResponse, Product } from '../app/products/model';
// import { catchError, retry } from 'rxjs/operators'
// import { throwError } from 'rxjs';


@Injectable()
export class BaseService {
    apiUrl = "http://localhost:4200/api/";
    constructor(private http: HttpClient){}

    getProducts() {
        return this.http.get<MyResponse>(this.apiUrl+"products/all");
    }
    updateProducts(data: Product) {
        return this.http.post<MyResponse>(this.apiUrl+"products/update",data);
    }
    deleteProducts(data: Product) {
        return this.http.post<MyResponse>(this.apiUrl+"products/delete",data);
    }
    addProducts(data: any) {
        return this.http.post<MyResponse>(this.apiUrl+"products",data);
    }
}